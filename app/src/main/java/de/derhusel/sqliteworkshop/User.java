package de.derhusel.sqliteworkshop;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class User {

    public static long add(UserDbHelper userDbHelper, String name, String surname, int age) {
        SQLiteDatabase db = userDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(UserContract.UserEntry.COLUMN_NAME_NAME, name);
        values.put(UserContract.UserEntry.COLUMN_NAME_SURNAME, surname);
        values.put(UserContract.UserEntry.COLUMN_NAME_AGE, age);

        return db.insert(UserContract.UserEntry.TABLE_NAME, null, values);
    }

    public static String getUserAll(UserDbHelper userDbHelper) {
        SQLiteDatabase db = userDbHelper.getReadableDatabase();
/*

        String[] projection = {
                UserContract.UserEntry._ID,
                UserContract.UserEntry.COLUMN_NAME_NAME,
                UserContract.UserEntry.COLUMN_NAME_SURNAME,
                UserContract.UserEntry.COLUMN_NAME_AGE
        };
*/

        Cursor cursor = db.query(
                UserContract.UserEntry.TABLE_NAME,
                null, //null will select all, otherwise user projection
                null,
                null,
                null,
                null,
                null
        );

        String ret = "";

        while(cursor.moveToNext()) {
            long userId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(UserContract.UserEntry._ID));

            String name = cursor.getString(
                    cursor.getColumnIndexOrThrow(UserContract.UserEntry.COLUMN_NAME_NAME));

            String surname = cursor.getString(
                    cursor.getColumnIndexOrThrow(UserContract.UserEntry.COLUMN_NAME_SURNAME));

            int age = cursor.getInt(
                    cursor.getColumnIndexOrThrow(UserContract.UserEntry.COLUMN_NAME_AGE));

            ret += "<User #"+userId+": "+ name + ", " + surname + ", " + age + ">\n";
        }

        cursor.close();

        return ret;
    }

    public static int deleteUserById(UserDbHelper userDbHelper, long id) {
        SQLiteDatabase db = userDbHelper.getWritableDatabase();

        String selection = UserContract.UserEntry._ID + " = ?";
        String[] selectionArgs = { "" + id };

        return db.delete(UserContract.UserEntry.TABLE_NAME, selection, selectionArgs);
    }
}
