package de.derhusel.sqliteworkshop;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    EditText input_name;
    EditText input_surname;
    EditText input_age;
    EditText input_id;
    Button button_save;
    Button button_delete;
    TextView txt_query_output;

    UserDbHelper userDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userDbHelper = new UserDbHelper(getApplicationContext());

        input_name = findViewById(R.id.input_name);
        input_surname = findViewById(R.id.input_surname);
        input_age = findViewById(R.id.input_age);
        input_id = findViewById(R.id.input_id);

        button_save = findViewById(R.id.button_save);
        button_delete = findViewById(R.id.button_delete);

        txt_query_output = findViewById(R.id.txt_query_output);

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sName = input_name.getText().toString();
                String sSurname = input_surname.getText().toString();
                String sAge = input_age.getText().toString();

                int iAge = Integer.parseInt(sAge);

                long ret = User.add(userDbHelper, sName, sSurname, iAge);
                if(ret < 0)
                    Toast.makeText(getApplicationContext(),"Failed to save User " + sSurname,Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getApplicationContext(),"Saved User " + sSurname + " at row " + ret,Toast.LENGTH_LONG).show();
                update_txt_query_output();
                clearInputs();
            }
        });

        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sId = input_id.getText().toString();

                int iId = Integer.parseInt(sId);

                int ret = User.deleteUserById(userDbHelper, iId);

                Toast.makeText(getApplicationContext(),"Deleted User #" + ret,Toast.LENGTH_LONG).show();

                update_txt_query_output();
                clearInputs();
            }
        });

        update_txt_query_output();
    }

    private void update_txt_query_output() {
        txt_query_output.setText(User.getUserAll(userDbHelper));
    }

    private void clearInputs() {
        input_surname.getText().clear();
        input_name.getText().clear();
        input_age.getText().clear();
        input_id.getText().clear();
    }
}
